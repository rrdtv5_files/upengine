#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <map>
#include <stdio.h>

struct state
{
	int pos_x;
	int pos_y;
	bool collision;
	std::string name;
	state(int pos_x_, int pos_y_, bool collision_, std::string name_)
	{
		pos_x = pos_x_;
		pos_y = pos_y_;
		collision_= collision_;
		name = name_;
	}
};

class level
{
	int size_x;
	int size_y;
	bool end_game=false;
	std::vector<std::vector<state>> grid;
	state player{1,1,true,"player"};

	public:

	level(int size_x_, int size_y_)
	{
		size_x = size_x_;
		size_y = size_y_;
		__occupy_grid__();
		//__register_input__();
		main_loop(0);
	}

	private:

	void __occupy_grid__()
	{
		bool coll;
		std::string name;
		for(int i=0; i < size_x; i++)
		{
			std::vector<state> row;
			for(int j=0; j < size_y; j++)
			{
				if(i == 0 || i == size_x-1 || j ==0 || j==size_y-1)
				{
					name = "wall";
					coll = true;
				}
				else
				{
					name = "floor";
					coll = false;
				}
				state sta{i,j,coll, name};
				row.push_back(sta);
			}
			grid.push_back(row);
		}
	}
	
	void __modify_tile__(state sta)
	{
		int pos_x = sta.pos_x;
		int pos_y = sta.pos_y;
		bool collision = sta.collision;
		std::string name = sta.name;
		grid[pos_x][pos_y] = sta;
	}
	
	char __register_input__()
	{
		char key;
		system("stty raw -echo");
		while(true)
		{
			if(end_game == true)
			{
				break;
			}
			else
			{
				key = getchar();
				return key;
				//std::cin >> key;
				/*
				if(key=='w')
				{
					player.pos_x -= 1;
					std::cout << "["<< player.pos_x<<", "<< player.pos_y << "]" << std::endl;
				}
				if(key=='a')
				{
					//std::cout << key << std::endl;
					player.pos_y -= 1;
					std::cout << "["<< player.pos_x<<", "<< player.pos_y << "]" << std::endl;
				}
				if(key=='s')
				{
					//std::cout << key << std::endl;
					player.pos_x += 1;
					std::cout << "["<< player.pos_x<<", "<< player.pos_y << "]" << std::endl;
				}
				if(key=='d')
				{
					//std::cout << key << std::endl;
					player.pos_y += 1;
					std::cout << "["<< player.pos_x<<", "<< player.pos_y << "]" << std::endl;
				}
				if(key=='q')
				{
					//std::cout << key << std::endl;
					end_game = true;
					system("stty cooked echo");
				}
				if(key=='e')
				{
					std::cout << "Using item at tile ["<< player.pos_x<<", "<< player.pos_y << "]" << std::endl;
				}
			*/
			}
		}
	}
	
	bool __check_overlap__(state sta1, state sta2)
	{
		int pos_x1 = sta1.pos_x;
		int pos_y1 = sta1.pos_y;
		int pos_x2 = sta2.pos_x;
		int pos_y2 = sta2.pos_y;
		if((pos_x1 == pos_x2) && (pos_y1 == pos_y2))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	bool __check_collision__(state sta1, state sta2)
	{
		 bool overlap = __check_overlap__(sta1, sta2);
		if(overlap && sta1.collision==true && sta2.collision == true)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	state update(state sta, char key)
	{
		int xdiv=0;
		int ydiv=0;
		if(key=='w')
		{
			ydiv = -1;
		}
		else if(key=='a')
		{
			xdiv = -1;
		}
		else if(key=='s')
		{
			ydiv = 1;
		}
		else if(key=='d')
		{
			xdiv = 1;
		}
		else if(key=='q')
		{
			end_game = true;
		}
		else if(key=='e')
		{
			std::cout << "action at ["<<sta.pos_x<<","<<sta.pos_y<<"]"<<std::endl;
		}
		int x = sta.pos_x + xdiv;
		int y = sta.pos_y + ydiv;
		state sta_new{x,y,sta.collision,sta.name};
		return sta_new;
	}

	void main_loop(float delta)
	{
		char key;
		// need to find method to do this without recreating struct manually each time
		state player_prev = player;
		while(true)
		{
			key = __register_input__();
			if(key == 'q')
			{
				break;
			}
			player = update(player, key);
			if (__check_collision__(player, grid[player.pos_x][player.pos_y]))
			{
				std::cout << "Collision happened!" << std::endl;
				player = player_prev;
			}
		}
	}
};

int main()
{
	level test(5,5);
	return 0;
}
